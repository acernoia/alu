#ifndef ALU_HPP
#define ALU_HPP

SC_MODULE(ALU)
{
   sc_in<sc_uint<16> >  src1;
   sc_in<sc_uint<16> >  src2;
   sc_in<sc_uint<2> >   func_alu;
   sc_out<sc_uint<16> > alu_out;

   SC_CTOR(ALU)
   {
     SC_THREAD(calcola);
     sensitive << src1 << src2 << func_alu;
   }
   private:
   void calcola ();
};


#endif
