#include <systemc.h>
#include "ALU.hpp"

using namespace std;

void ALU::calcola()
{
   sc_uint<16> op1;
   sc_uint<16> op2;
   sc_uint<2> function;
   sc_uint<16> res;
   while(true)
   {
     wait();
     op1=src1->read();
     op2=src2->read();
     switch (func_alu->read())
     {
      case 0 : res=op1+op2; break;    //ADD
      case 1 : res=~(op1&op2); break; //NAND
      case 2 : res=op1;     break;    //PASS1
      case 3 : if (op1==op2)
	         res=1;
               else
                 res=0;
               break;                 //EQ?
     }
     alu_out->write(res);
   }
}
