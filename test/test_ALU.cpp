#include <systemc.h>
#include <string>
#include "ALU.hpp"

using namespace std;

SC_MODULE(TestBench)
{
    sc_signal<sc_uint<16> > operand1;
    sc_signal<sc_uint<16> > operand2;
    sc_signal<sc_uint<2> >  controllo;
    sc_signal<sc_uint<16> > result;
    ALU ALU1;
    
    SC_CTOR(TestBench) : ALU1("ALU1")
    {
        SC_THREAD(stimulus_thread);
        ALU1.src1(this->operand1);
        ALU1.src2(this->operand2);
        ALU1.alu_out(this->result);
        ALU1.func_alu(this->controllo);
        init_values();
    }

    int check() 
    {

        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            if (dato_letto[i] != risultato_test[i])
            {
                cout << "TEST FALLITO: " << i << endl;
                cout << "RISULTATO TEORICO: " << risultato_test[i] << endl;
                cout << "RISULTATO TEST :" << dato_letto[i] << endl;
                return 1;
             }
        }
        cout << "TEST OK" << endl;                 
        return 0;
    }


  private:

   void stimulus_thread() 
   {
        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            operand1.write(op1_test[i]);
            cout << "Operando 1 = " << op1_test[i] << endl;
            operand2.write(op2_test[i]);
            cout << "Operando 2 = " << op2_test[i] << endl;
            controllo.write(controllo_test[i]);
            cout << "Funzione = ";
            switch (controllo_test[i])
            {
              case 0 : cout << "ADD" << endl; break;
              case 1 : cout << "NAND" << endl; break;
              case 2 : cout << "PASS1" << endl; break;
              case 3 : cout << "EQ?" << endl; break;
             }
            wait(1,SC_NS);
            dato_letto[i] = result.read(); 
            cout << "DATO LETTO : " << dato_letto[i] << endl << endl;
        }
    }

    static const unsigned TEST_SIZE = 8;
    // NOTA: short = 2 byte
    unsigned short op1_test[TEST_SIZE];
    unsigned short op2_test[TEST_SIZE];
    unsigned short controllo_test[TEST_SIZE];
    unsigned short risultato_test[TEST_SIZE];
    unsigned short dato_letto[TEST_SIZE];

    void init_values() 
    {
        op1_test[0] = 1;
        op1_test[1] = 5;
        op1_test[2] = 0;
        op1_test[3] = 4;
        op1_test[4] = 0;
        op1_test[5] = 239;
        op1_test[6] = 666;
        op1_test[7] = 23;

        op2_test[0] = 0;
        op2_test[1] = 3;
        op2_test[2] = 0;
        op2_test[3] = 0;
        op2_test[4] = 2;
        op2_test[5] = 2;
        op2_test[6] = 666;
        op2_test[7] = 37;

        controllo_test[0]=0;
        controllo_test[1]=0;
        controllo_test[2]=1;
        controllo_test[3]=1;
        controllo_test[4]=2;
        controllo_test[5]=2;
        controllo_test[6]=3;
        controllo_test[7]=3;

        for (unsigned i=0;i<TEST_SIZE;i++)
        {
          switch (controllo_test[i])
          {
            case 0 : risultato_test[i]=op1_test[i]+op2_test[i]; break; //ADD
            case 1 : risultato_test[i]=~(op1_test[i]&op2_test[i]); break; //NAND
            case 2 : risultato_test[i]=op1_test[i]; break; //PASS1
            case 3 : if (op1_test[i]==op2_test[i])
	               risultato_test[i]=1;
                     else
                       risultato_test[i]=0;
                      break;     //EQ?
          }
        }
    }


};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");

  cout << "START TEST" << endl << endl;

  sc_start();

  return test.check();
}
